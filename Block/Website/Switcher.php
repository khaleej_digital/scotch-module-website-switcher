<?php

namespace Redbox\WebsiteSwitcher\Block\Website;

/**
 * @category  RedboxDigital
 * @package   Redbox_WebsiteSwitcher
 * @author    Bolaji Olubajo <bolaji.tolulope@redboxdigital.com>
 * @copyright Copyright (c) 2015 Redbox Digital (http://www.redboxdigital.com)
 *
 * Website switcher block
 */

use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Store\Model\Website;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Data\Helper\PostHelper;
use Redbox\WebsiteSwitcher\Helper\Config as ConfigHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Directory\Model\CountryFactory;
/**
 * Class Switcher
 * @package Redbox\WebsiteSwitcher\Block\Website
 */
class Switcher extends \Magento\Store\Block\Switcher
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var FilterProvider
     */
    protected $filterProvider;

     /**
     * @var CountryFactory;

     */
    protected $countryFactory;

    /**
     * Switcher constructor.
     *
     * @param Context        $context
     * @param PostHelper     $postDataHelper
     * @param ConfigHelper   $configHelper
     * @param BlockFactory   $blockFactory
     * @param FilterProvider $filterProvider
     * @param array          $data
     */
    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        ConfigHelper $configHelper,
        BlockFactory $blockFactory,
        FilterProvider $filterProvider,
        CountryFactory $countryFactory,
        array $data = []
    )
    {
        $this->blockFactory = $blockFactory;
        $this->filterProvider = $filterProvider;
        $this->configHelper = $configHelper;
        $this->countryFactory = $countryFactory;
        parent::__construct($context, $postDataHelper, $data);
    }

    /**
     * @return \Magento\Store\Api\Data\WebsiteInterface[]
     */
    public function getWebsites()
    {
        $availableWebsites = [];
        if ($websitesIds = $this->configHelper->getSpecificWebsites()) {
            $availableWebsitesArr = explode(',', $websitesIds);
            foreach ($availableWebsitesArr as $availableWebsiteId) {
                try {
                    $availableWebsites[] = $this->_storeManager->getWebsite($availableWebsiteId);
                } catch (NoSuchEntityException $e) {
                    $this->_logger->error(sprintf(
                            'Website switcher - error loading website from the configuration. Wrong website id - %s', $availableWebsiteId
                        )
                    );
                }
            }
        }

        return ($availableWebsites) ? $availableWebsites : $this->_storeManager->getWebsites();
    }

    /**
     * @return \Magento\Store\Model\Website
     */
    public function getCurrentWebsite()
    {
        return $this->_storeManager->getWebsite();
    }

    /**
     * @param  Website $website
     * @return string
     */
    public function getWebsiteLabel(Website $website)
    {
        if ($this->configHelper->isCountryNameEnabled($website->getCode())) {
            return $this->configHelper->getWebsiteCountryName($website);
        }
        return $website->getName();
    }

    public function isCurrentWebsiteEnabled()
    {
        $websiteId = $this->getCurrentWebsite()->getId();
        $websiteIds =  $this->configHelper->getSpecificWebsites();
        $enabledWebsitesArr = explode(',', $websiteIds);
        if(in_array($websiteId, $enabledWebsitesArr)){
            return true;
        }
        return false;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getCurrentWebsiteCode()
    {
        return $this->getCurrentWebsite()->getCode();
    }

    /**
     * @return string
     * @deprecated
     */
    public function getCurrentWebsiteName()
    {
        return $this->_storeManager->getWebsite()->getName();
    }

    /**
     * @param Website $website
     * @return string
     */
    public function getDefaultStoreBaseUrl(Website $website)
    {
        return $website->getDefaultStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_LINK,
            $this->getRequest()->isSecure()
        );
    }

    /**
     * Check whether to use the store switch controller-action (if true)
     * or link directly to the default store's base URL (if false).
     *
     * Always returns false out of the box for back compat @ 0.1.*. Return
     * true with an 'after' plugin to use the controller.
     *
     * @return bool
     */
    public function useStoreSwitchController()
    {
        return false;
    }

    /**
     * Check config settings to enable dialog
     *
     * @return bool
     */
    public function isDialogEnabled()
    {
        return $this->configHelper->isDialogEnabled();
    }

    /**
     * @return array
     */
    public function getCmsBlockData()
    {
        $result = [
            'title' => '',
            'content' => '',
        ];

        $storeId = $this->_storeManager->getStore()->getId();
        /**
         * @var \Magento\Cms\Model\Block $block
         */
        $block = $this->blockFactory->create();
        $block->setStoreId($storeId)->load(ConfigHelper::CMS_BLOCK_IDENTIFIER, 'identifier');
        if ($block->isActive()) {
            $result['content'] = $this->filterProvider->getBlockFilter()
                ->setStoreId($storeId)
                ->filter($block->getContent());
            $result['title'] = $block->getTitle();
        }

        return $result;
    }
}
