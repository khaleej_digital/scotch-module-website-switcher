<?php
/**
 * @category  RedboxDigital
 * @package   Redbox_WebsiteSwitcher
 * @author    Joseph McDermott <code@josephmcdermott.co.uk>
 * @copyright Copyright (c) 2016 Redbox Digital (http://www.redboxdigital.com)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Redbox_WebsiteSwitcher',
    __DIR__
);
