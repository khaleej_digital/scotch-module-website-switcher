<?php
namespace Redbox\WebsiteSwitcher\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Website;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Helper\Context;

/**
 * Class Config
 * @package Redbox\WebsiteSwitcher\Helper
 */
class Config extends AbstractHelper
{
    /**
     * Enable or disable website switcher dialog popup
     */
    const XML_DIALOG_ENABLED = 'websiteswitcher/general/enabled';

    /**
     * Enable or disable website switcher dialog popup
     */
    const SHOW_WEBSITE_COUNTRIES = 'websiteswitcher/general/enable_country';

    /**
     * Identified of CMS Block, where dialog content is stored
     */
    const CMS_BLOCK_IDENTIFIER = 'redbox_website_switcher';

    /**
     * Path to specificwebsites configuration
     */
    const XML_SPEC_WEBSITES = 'websiteswitcher/general/specificwebsites';

     /**
     * Path to magento country configuration
     */
    const DEFAULT_COUNTRY_CONFIG_PATH = 'general/country/default';

    /**
     * @var CountryFactory;

     */
    protected $countryFactory;

    public function __construct(Context $context, CountryFactory $countryFactory)
    {
        parent::__construct($context);
        $this->countryFactory = $countryFactory;
    }

    /**
     * @return mixed
     */
    public function isDialogEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_DIALOG_ENABLED);
    }

    /**
     * @return mixed
     */
    public function getSpecificWebsites()
    {
        return $this->scopeConfig->getValue(self::XML_SPEC_WEBSITES);
    }

    /**
     * @param null|int|string $website
     */
    public function isCountryNameEnabled($website = null)
    {
        if (!is_null($website)) {
            $this->scopeConfig->getValue(
                self::SHOW_WEBSITE_COUNTRIES, 
                ScopeInterface::SCOPE_WEBSITE,
                $website
            );
        }

        return $this->scopeConfig->getValue(self::SHOW_WEBSITE_COUNTRIES);
    }

    /**
     *
     * @return string
     */
    public function getWebsiteCountryName(Website $website)
    {
        $countryCode = $this->scopeConfig->getValue(
            self::DEFAULT_COUNTRY_CONFIG_PATH, 
            ScopeInterface::SCOPE_WEBSITE,
            $website->getId()
        );
        $country = $this->countryFactory->create()->loadByCode($countryCode);
       
        return $country->getName();
    }
}
