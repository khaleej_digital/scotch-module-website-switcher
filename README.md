# Redbox_WebsiteSwitcher

This module adds a website switcher to the header of multi-website Magento installation.

## Apache Configuration

This module will not work fully if the server (or Magento index.php) is not altered to pick up the store code cookie. Here's a sample that works in Apache 2.4:

```
SetEnvIf Cookie "store=([a-z]+)" MAGE_RUN_CODE=$1
SetEnvIf Cookie "store=[a-z]+" MAGE_RUN_TYPE=store
```

## To do

- Some minor design changes may be required.

## Contributors

- [Bolaji Olubajo](mailto:bolaji.tolulope@redboxdigital.com)
- [Aneurin "Anny" Barker Snook](mailto:aneurin.barkersnook@redboxdigital.com)
