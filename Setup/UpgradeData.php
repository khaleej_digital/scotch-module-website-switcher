<?php
namespace Redbox\WebsiteSwitcher\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Redbox\Install\Model\CmsHelper;
use Redbox\WebsiteSwitcher\Helper\Config as ConfigHelper;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var CmsHelper
     */
    protected $cmsHelper;

    /**
     * UpgradeData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param CmsHelper       $cmsHelper
     */
    public function __construct(EavSetupFactory $eavSetupFactory, CmsHelper $cmsHelper)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->cmsHelper = $cmsHelper;
    }

    /**
     * Upgrades data for a module
     *
     * @param  ModuleDataSetupInterface $setup
     * @param  ModuleContextInterface   $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            // Insert CMS Block for website switcher
            $blockData = [
                'identifier' => ConfigHelper::CMS_BLOCK_IDENTIFIER,
                'title' => 'Please note',
            ];
            $blockContent = "
                  <p>IF YOU CHANGE THE STORE LANGUAGE / CURRENCY, YOUR SHOPPING BASKET WILL BE CLEARED.</p>
                  <p>WOULD YOU LIKE TO CONTINUE?</p>
            ";

            $this->cmsHelper->createBlock($blockData['identifier'], $blockContent, $blockData);
        }

        $setup->endSetup();
    }
}
