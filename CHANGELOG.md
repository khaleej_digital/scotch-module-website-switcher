# Redbox_WebsiteSwitcher changelog

## 0.1.2

Add `useStoreSwitchController()` method to enable HTTP navigation method override by plugin.

## 0.1.1

Add `getWebsiteLabel()` method to enable label overriding by plugin.

## 0.1.0

Initial version.
