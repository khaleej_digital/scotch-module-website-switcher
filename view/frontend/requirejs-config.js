/**
 * @package   Redbox_Navigation
 * @copyright Copyright (c) 2015 Redbox Digital (http://www.redboxdigital.com)
 */

var config = {
    map: {
        '*': {
            switcherPopup: 'Redbox_WebsiteSwitcher/js/popup',
        }
    }
};